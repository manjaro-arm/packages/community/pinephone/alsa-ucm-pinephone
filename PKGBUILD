# Maintainer: Philip Müller <philm@manjaro.org>
pkgname=alsa-ucm-pinephone
pkgver=0.5.8
pkgrel=4
pkgdesc="UCM files for PinePhone (Pro)"
arch=(any)
url="https://gitlab.com/pine64-org/pine64-alsa-ucm"
license=('BSD-3-Clause')
depends=('alsa-ucm-conf>=1.2.9')
replaces=('alsa-ucm-pinephone-pro')
provides=('alsa-ucm-pinephone-pro')
conflicts=('alsa-ucm-pinephone-pro')
install=install
source=(PinePhone-HiFi.conf PinePhone-VoiceCall.conf PinePhone.conf
        PinePhonePro-HiFi.conf PinePhonePro-VoiceCall.conf PinePhonePro.conf
        alpm.hook)

package() {
	# These UCMs came from Mobian
	install -D -m644 "$srcdir"/PinePhonePro.conf \
		"$pkgdir"/usr/share/alsa/ucm2/PinePhonePro/PinePhonePro.conf
	install -D -m644 "$srcdir"/PinePhonePro-HiFi.conf \
		"$pkgdir"/usr/share/alsa/ucm2/PinePhonePro/HiFi.conf
	install -D -m644 "$srcdir"/PinePhonePro-VoiceCall.conf \
		"$pkgdir"/usr/share/alsa/ucm2/PinePhonePro/VoiceCall.conf
	# Link for 1.2.6 and higher
	mkdir -p "$pkgdir"/usr/share/alsa/ucm2/conf.d/simple-card
	ln -sfv /usr/share/alsa/ucm2/PinePhonePro/PinePhonePro.conf \
	        "$pkgdir"/usr/share/alsa/ucm2/conf.d/simple-card/PinePhonePro.conf
	# Install only the config files
	# see also: https://gitlab.com/postmarketOS/pmaports/-/issues/2115
	install -D -m644 "$srcdir"/PinePhone.conf \
		"$pkgdir"/usr/share/alsa/ucm2/PinePhone/PinePhone.conf
	install -D -m644 "$srcdir"/PinePhone-HiFi.conf \
		"$pkgdir"/usr/share/alsa/ucm2/PinePhone/HiFi.conf
	install -D -m644 "$srcdir"/PinePhone-VoiceCall.conf \
		"$pkgdir"/usr/share/alsa/ucm2/PinePhone/VoiceCall.conf
	# Install alpm hook
	install -D -m644 "$srcdir"/alpm.hook \
		"$pkgdir"/usr/share/libalpm/hooks/"$pkgname".hook
}

md5sums=('64eadcf297a42693cd59a5bd84df5c8e'
         'd091b0a80a7cac0e55fcc43449ba4b6f'
         '7077c0a82a8ac5f4bda661a13ad9e7af'
         '10a97508e34180d22fb3d6139dc2b090'
         '775c706f02eab8038314ff1f2924548f'
         '5820136828a66b30732d56006a58f0f1'
         '741b4de7605e22fe45b88fc6e68b3353')
